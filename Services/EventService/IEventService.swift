//
//  IEventService.swift
//  Crowded
//
//  Created by adb on 4/7/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit
import RxSwift

protocol IEventService {
    
    func getList(page: Int, coordinate: CLLocationCoordinate2D?) -> Single<EventList>
    
    func getEvent(id: String) -> Single<EventDto?>
    
    func apply(applyInput: ApplyInput) -> Single<NZBaseResponse>
    
    func active(applyInput: ApplyInput) -> Single<NZBaseResponse>
    
    func pause(applyInput: ApplyInput) -> Single<NZBaseResponse>
    
    func continueEvent(applyInput: ApplyInput) -> Single<NZBaseResponse>
    
    func left(applyInput: ApplyInput) -> Single<NZBaseResponse>
    
    func setStatus(payload: SetStatusInput) -> Single<NZBaseResponse>
    
    func currentEvent() -> Single<EventDto?>
    
    func rateEvent(payload: FeedbackInput) -> Single<NZBaseResponse>
    
    func leftOptions() -> Single<[String]>
    
    func leftEvent(payload: LeftFeedbackInput) -> Single<NZBaseResponse>
    
    func elapsed(payload: ElapsedInput) -> Single<NZBaseResponse>
    
    func getMyEvents(page: Int, previous: Bool , date: Int?) -> Single<MyEventList>
}
