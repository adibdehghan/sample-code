//
//  EventService.swift
//  Crowded
//
//  Created by adb on 4/7/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit
import RxSwift

class EventService: NZNetwork, IEventService {
    
    func getList(page: Int = 0, coordinate: CLLocationCoordinate2D? = nil) -> Single<EventList>
    {
        return Single.deferred {
            NZNetwork.get(path: NetworkRouter.routes.event.getList(page: page, lat: coordinate != nil ? coordinate?.latitude.string:nil, lon: coordinate != nil ? coordinate?.longitude.string : nil)) { (data) -> (Single<EventList>) in
                let result = try JSONDecoder().decode(EventListOutput.self, from: data)
                return Single.just(result.eventList)
            }
        }
    }
    
    func getEvent(id: String) -> Single<EventDto?>
    {
        NZNetwork.get(path: NetworkRouter.routes.event.get(id: id)) { (data) -> (Single<EventDto?>) in
            let result = try JSONDecoder().decode(EventOutput.self, from: data)
            return Single.just(result.event)
        }
    }
    
    func getMyEvents(page: Int = 0, previous: Bool, date: Int? = nil) -> Single<MyEventList>
    {
        NZNetwork.get(path: NetworkRouter.routes.event.myEvents(page: page, previous: previous,date: date)) { (data) -> (Single<MyEventList>) in
            let result = try JSONDecoder().decode(MyEventOutput.self, from: data)
            return Single.just(result.eventList)
        }
    }
    
    func apply(applyInput: ApplyInput) -> Single<NZBaseResponse>
    {
        NZNetwork.post(path: NetworkRouter.routes.event.apply, payload: applyInput) { (data) -> (Single<NZBaseResponse>) in
              let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
              return Single.just(result)
          }
    }
    
    func active(applyInput: ApplyInput) -> Single<NZBaseResponse>
    {
        NZNetwork.post(path: NetworkRouter.routes.event.active, payload: applyInput) { (data) -> (Single<NZBaseResponse>) in
            let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
            return Single.just(result)
        }
    }
    
    func pause(applyInput: ApplyInput) -> Single<NZBaseResponse>
    {
        return Single.deferred {
            NZNetwork.post(path: NetworkRouter.routes.event.pause, payload: applyInput) { (data) -> (Single<NZBaseResponse>) in
                let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
                return Single.just(result)
            }
        }
    }
    
    func continueEvent(applyInput: ApplyInput) -> Single<NZBaseResponse>
    {
        return Single.deferred {
            NZNetwork.post(path: NetworkRouter.routes.event.continueEvent, payload: applyInput) { (data) -> (Single<NZBaseResponse>) in
                let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
                return Single.just(result)
            }
        }
    }
    
    func left(applyInput: ApplyInput) -> Single<NZBaseResponse>
    {
        NZNetwork.post(path: NetworkRouter.routes.event.left, payload: applyInput) { (data) -> (Single<NZBaseResponse>) in
               let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
               return Single.just(result)
           }
     }
    
    func setStatus(payload: SetStatusInput) -> Single<NZBaseResponse>
    {
        NZNetwork.post(path: NetworkRouter.routes.event.setStatus, payload: payload) { (data) -> (Single<NZBaseResponse>) in
              let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
              return Single.just(result)
          }
    }
    
    func currentEvent() -> Single<EventDto?>
    {
        NZNetwork.get(path: NetworkRouter.routes.event.current) { (data) -> (Single<EventDto?>) in
            let result = try JSONDecoder().decode(EventOutput.self, from: data)
            return Single.just(result.event)
        }
    }
    
    func rateEvent(payload: FeedbackInput) -> Single<NZBaseResponse>
    {
        NZNetwork.post(path: NetworkRouter.routes.event.feedback, payload: payload) { (data) -> (Single<NZBaseResponse>) in
              let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
              return Single.just(result)
          }
    }
    
    func leftEvent(payload: LeftFeedbackInput) -> Single<NZBaseResponse>
     {
         NZNetwork.post(path: NetworkRouter.routes.event.leftFeedback, payload: payload) { (data) -> (Single<NZBaseResponse>) in
               let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
               return Single.just(result)
           }
     }
    
    func leftOptions() -> Single<[String]>
    {
        NZNetwork.get(path: NetworkRouter.routes.event.leftOption) { (data) -> (Single<[String]>) in
            let result = try JSONDecoder().decode(LeftOptionOutput.self, from: data)
            return Single.just(result.optionList)
        }
    }
    
    func elapsed(payload: ElapsedInput) -> Single<NZBaseResponse>
      {
          NZNetwork.post(path: NetworkRouter.routes.event.elapsed, payload: payload) { (data) -> (Single<NZBaseResponse>) in
                let result = try JSONDecoder().decode(NZBaseResponse.self, from: data)
                return Single.just(result)
            }
      }
}
