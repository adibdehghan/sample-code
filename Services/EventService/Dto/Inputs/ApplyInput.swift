//
//  ApplyInput.swift
//  Crowded
//
//  Created by adb on 4/12/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

struct ApplyInput: Codable {
    var eventId: String
}
