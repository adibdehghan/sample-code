//
//  ElapsedInput.swift
//  Crowded
//
//  Created by adb on 4/15/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

struct ElapsedInput: Codable {
    var eventId: String
    var elapsed: Int
    var coordinates: Array<String>
    var isFinished: Bool
}
