//
//  LeftFeedBackInput.swift
//  Crowded
//
//  Created by adb on 4/14/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

struct LeftFeedbackInput: Codable {
    var eventId: String
    var title: String
    var desc: String?
}
