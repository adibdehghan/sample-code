//
//  SetStatusInput.swift
//  Crowded
//
//  Created by adb on 4/13/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

struct SetStatusInput: Codable {

    var eventId: String
    var status: String
}
