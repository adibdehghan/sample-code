//
//  LeftOptionOutput.swift
//  Crowded
//
//  Created by adb on 4/14/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

public class LeftOptionOutput: NZBaseResponse {
    private enum CodingKeys: String, CodingKey { case items}
    var optionList: [String]
    
    required init(from decoder: Decoder) {
        let container = try! decoder.container(keyedBy: RootKeys.self)
       optionList = try! container.decode([String].self, forKey: .data)        
        super.init(from: decoder)
    }
}
