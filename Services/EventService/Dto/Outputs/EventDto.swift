//
//  EventDto.swift
//  Crowded
//
//  Created by adb on 4/8/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

class EventDto: Codable
{
    var id: String
    var title : String
    var desc : String
    var status : String?
    var attendance: Int
    var images: [Image]
    var value: String
    var date: MDate        
    var address : String
    var map: Map?
    var allowedRadius: Int?
    var coordinates: [Double]?
}
