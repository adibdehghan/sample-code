//
//  MyEventList.swift
//  Crowded
//
//  Created by adb on 4/18/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

struct MyEventList  {
    var nextPage: Int?
    var events: [MyEventDto]
}
