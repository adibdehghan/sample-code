//
//  MyEventDto.swift
//  Crowded
//
//  Created by adb on 4/18/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

struct MyEventDto: Codable {
    var id: String
    var title : String
    var image: Image
    var value: String
    var date: MDate
    var attendance: Int
    var userEventStatus: String
}
