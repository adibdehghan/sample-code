//
//  EventOutput.swift
//  Crowded
//
//  Created by adb on 4/7/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

public class EventOutput: NZBaseResponse {
    private enum CodingKeys: String, CodingKey { case item }
    var event: EventDto?
    
    required init(from decoder: Decoder) {
        let container = try! decoder.container(keyedBy: RootKeys.self)
        event = try? container.decode(EventDto.self, forKey: .data)

        super.init(from: decoder)
    }
}
