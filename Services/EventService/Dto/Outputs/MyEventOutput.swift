//
//  MyEventOutput.swift
//  Crowded
//
//  Created by adb on 4/18/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

public class MyEventOutput: NZBaseResponse {
    private enum CodingKeys: String, CodingKey { case items, nextPage}
    var eventList: MyEventList = MyEventList(nextPage: 0, events: [])
    
    required init(from decoder: Decoder) {
        let container = try! decoder.container(keyedBy: RootKeys.self)
        let dataContainer = try! container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        eventList.nextPage = try? dataContainer.decode(Int?.self, forKey: .nextPage) ?? nil
        eventList.events = try! dataContainer.decode([MyEventDto].self, forKey: .items)
        
        super.init(from: decoder)
    }
}
