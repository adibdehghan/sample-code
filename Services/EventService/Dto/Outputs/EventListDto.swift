//
//  EventListDto.swift
//  Crowded
//
//  Created by adb on 4/9/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

struct EventListDto: Codable
{
    var id: String
    var title : String    
    var image: Image
    var value: String
    var date: MDate
    var area: String
}
