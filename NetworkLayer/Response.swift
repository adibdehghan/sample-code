//
//  Response.swift
//  Crowded
//
//  Created by adb on 3/29/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

public class Response: NZBaseResponse {
    private enum CodingKeys: String, CodingKey { case access_token, access_type}
    
    var access_token: String
    var access_type: String
    
    required init(from decoder: Decoder) {
        let container = try! decoder.container(keyedBy: RootKeys.self)
        let dataContainer = try! container.nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        
        access_token = try! dataContainer.decode(String.self, forKey: .access_token)
        access_type = try! dataContainer.decode(String.self, forKey: .access_type)
        
        super.init(from: decoder)
    }
}

