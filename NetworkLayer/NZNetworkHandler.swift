//
//  NetworkRouter.swift
//  Crowded
//
//  Created by adb on 3/29/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit
import RxSwift

enum NZNetworkMethod : String {
    case GET
    case POST
    case PUT
    case DELETE
}

public extension NSError {
    struct app {
        public struct network {
            static let domain = "NZNetwork"
            
            public static func serverError(statusCode: Int, message: String = "") -> NSError {
                NSError(domain: domain, code: statusCode, userInfo: [NSLocalizedDescriptionKey: "\(message)"])
            }
            public static func authResponseDecode() -> NSError {
                NSError(domain: domain, code: -100, userInfo: [NSLocalizedDescriptionKey: "Could not json decode the response for auth. request!"])
            }
            public static func googleLoginSuccessNoUser() -> NSError {
                NSError(domain: domain, code: -101, userInfo: [NSLocalizedDescriptionKey: "GIDSignIn finished without any error or any user returned!"])
            }
        }
    }
}

class NZNetworkHandler: NZSingleton {
    
    public struct K {
        static let key1 = "key1"
        static let key2 = "*****c6InZI"
        static let endpoint = nz._confString("$endpoint", default: "api.endpoint")
        static let endpoint_protocol = nz._confString("$endpoint_protocol", default: "https")
        
        static let addRandomQuery = true
        static let randomMax = UInt32(1000)
        static let maxTimeout = TimeInterval(15) //default max timeout
    }
    
    var session: URLSession!
    let tokenAQSubject = PublishSubject<Void>()
    var tokenAQDisposable: Disposable?
    required init() {
        super.init()
        
        reloadMemoryCache()
        
        let config = URLSessionConfiguration.default
        let versions = helper.app.versions() //ncs2 : make use helper.appVersions() in new ncs
        config.httpAdditionalHeaders = ["User-Agent": "NZNetwork/\(Bundle.main.bundleIdentifier ?? "")/\(versions.version.toString())/\(versions.build)"]
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        session = URLSession(configuration: config, delegate: nil, delegateQueue: nil)
    }
    
    class func serverAPIPathString(_ path: String) -> URL {
        
        
        let urlComps = URLComponents(string: "\(K.endpoint_protocol)://\(K.endpoint)")!

        let url = urlComps.url!
        let urlWithPath = url.appendingPathComponent(path).absoluteString.removingPercentEncoding
        
        return URL(string: urlWithPath!)!
    }
    
    func serverTask(path: String, method: NZNetworkMethod, payload: Encodable?, timeout: TimeInterval, retryOnNoConnection: Bool = true)->Single<Data> {
        if token != nil {
            return __serverTask(path: path, method: method, payload: payload, timeout: timeout, retryOnNoConnection: retryOnNoConnection)
        } else {
            requestForTokenIfNeeded()
            return tokenAQSubject.take(1).asSingle().flatMap { [weak self] () -> PrimitiveSequence<SingleTrait, Data> in
                guard let this = self else { fatalError("\(#function) self is null! catastrophic! abort...") }
                return this.__serverTask(path: path, method: method, payload: payload, timeout: timeout, retryOnNoConnection: retryOnNoConnection)
            }
        }
    }
    
    func __serverTask(path: String, method: NZNetworkMethod, payload: Encodable?, timeout: TimeInterval, retryOnNoConnection: Bool = true)->Single<Data> {
        
        return Single<Data>.create { [weak self] (observer) -> Disposable in
            let disposable  = CompositeDisposable()
            
            guard let this = self else { return disposable }
            
            let request = this.serverRequestWithPath(path: path, method: method, payload: payload, timeout: timeout)
            #if DEBUG
            print("NZNetwork: Sending [\(method.rawValue)] request to \(String(describing: request.url?.absoluteURL))")
            #endif
            
            let task = this.session.dataTask(with: request) { (data, response, error) in
                if error == nil {
                    let resp = response as! HTTPURLResponse
                    if resp.statusCode == 200 {
                        if let data = data {

                            observer(.success(data))
                        }
                    } else if resp.statusCode == 403 {
                        this.revokeAccessToken()
                        _ = disposable.insert(this.serverTask(path: path, method: method, payload: payload, timeout: timeout).subscribe(observer))
                        print("\(#function) 403")
                    }
                    else {
                        do {
                            let response = try JSONDecoder().decode(NZBaseResponse.self, from: data!)
                            observer(.error(NSError.app.network.serverError(statusCode: resp.statusCode, message: response._meta.msg ?? "null")))
                        } catch {
                            observer(.error(NSError.app.network.serverError(statusCode: resp.statusCode)))
                        }
                    }
                } else {
                    observer(.error(error!))
                }
            }
            
            task.resume()
            _ = disposable.insert(Disposables.create {
                task.cancel()
            })
            
            return disposable
        }.timeout(DispatchTimeInterval.seconds(Int(timeout)), scheduler: SerialDispatchQueueScheduler.init(qos: .default))
            /*
             Here we handle the internal errors came from the above stream. In some cases the NZNetwork shouldn't emit the error to the upstream and instead it should retry it. Like timeouts.
             */
            .retryWhen({ errors in
                return errors.flatMap { (error) -> Observable<Void> in
                    if error is RxSwift.RxError {
                        switch error {
                        case RxSwift.RxError.timeout: //if the error was emitted because of rx timeout, retry it immediately.
                            return Observable.just(())
                        default:
                            return Observable.error(error)
                        }
                    } else {
                        let error = error as NSError
                        if retryOnNoConnection, error.domain == NSURLErrorDomain, error.code == NSURLErrorNotConnectedToInternet { // if the error was emitted because of no internet connection, wait until the connection becomes available and then retry.
                            return NZReachability.instance().observable.filter { (reachability) -> Bool in
                                return reachability.connection != .none
                            }.take(1).map { (reachability) -> Void in
                                return ()
                            }
                        } else {
                            return Observable.error(error)
                        }
                    }
                }
            }).subscribeOn(SerialDispatchQueueScheduler.init(qos: .default)).observeOn(SerialDispatchQueueScheduler.init(qos: .default))
    }
    
    
    /// Just fools to JSONEncoder() to encode a simple Encodable for us!
    struct EncodableWrapper: Encodable {
        let wrapped: Encodable
        
        func encode(to encoder: Encoder) throws {
            try self.wrapped.encode(to: encoder)
        }
    }
    
    
    func serverRequestWithPath(path: String, method: NZNetworkMethod, payload: Encodable?, timeout: TimeInterval) -> URLRequest {
        
        var request = URLRequest(url: NZNetworkHandler.serverAPIPathString(path), cachePolicy: session.configuration.requestCachePolicy, timeoutInterval: timeout)
                
        
        request.addValue("application/json", forHTTPHeaderField: "accept")
        
        if token != nil {
            request.addValue(token!, forHTTPHeaderField: "x-token")
        }
        //        request.addValue("WhatShouldComeHere?!", forHTTPHeaderField: "x-dontcheckme")
        
        let randomReqIdHash = randomString(length: 32)
        
        var hashing = String(randomReqIdHash)
        
        request.addValue(randomReqIdHash, forHTTPHeaderField: "x-reqid")
        
        let versions = helper.app.versions()
        
        request.addValue("ios", forHTTPHeaderField: "nizek-ios")
      
        request.addValue(versions.version.toString(), forHTTPHeaderField: "nizek-version")
      
        request.addValue("\(versions.build)", forHTTPHeaderField: "nizek-build")
      
        #if DEBUG
        request.addValue("debug", forHTTPHeaderField: "nizek-env")
        #else
        request.addValue("release", forHTTPHeaderField: "nizek-env")
        #endif
        
        request.addValue(UIDeviceHardware.platform(), forHTTPHeaderField: "nizek-model")
        request.addValue("apple", forHTTPHeaderField: "nizek-manufacturer")
        
        let l = nz.getCurrentLangIdent
        var lang = "en"
        if Utilz.str_ok2(l) {
            lang = l!
        }
        
        request.addValue(lang.lowercased(), forHTTPHeaderField: "lang")
        
        request.httpMethod = method.rawValue
        
        switch method {
        case .POST: // Our posts should always have json body even if you pass Data as the payload
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            guard let obj = payload else { fatalError("Payload cannot be null in POST") }
            if obj is Data {
                let data = obj as! Data
                request.addValue(String(data.count), forHTTPHeaderField: "Content-Length")
                request.httpBody = data
            }
            else if obj is Dictionary<String, Encodable> {
                let dic = obj as! Dictionary<String, Encodable>
                let wrapped = dic.mapValues(EncodableWrapper.init(wrapped:))
                let data = try! JSONEncoder().encode(wrapped)
                request.addValue(String(data.count), forHTTPHeaderField: "Content-Length")
                request.httpBody = data
            }
            else { //it's a normal encodable! try to jsonEncode it!
                let data = obj.toJSONData()!
                let str = String(data: data, encoding: .utf8)!
                let hmac = str.hmac(algorithm: .SHA256, key: K.key1)
                let dt = "\(str).\(hmac)".data(using: .utf8)!
                //                let data = try? JSONEncoder().encode(obj)
                request.addValue(String(dt.count), forHTTPHeaderField: "Content-Length")
                request.httpBody = dt
                hashing.append("\(dt.count)")
            }

        case .PUT:
            guard let obj = payload else { fatalError("Payload cannot be null in PUT") }
            if obj is Data {

                let boundary = generateBoundaryString()
                request.setValue("multipart/form-data; boundary=" + boundary,
                           forHTTPHeaderField: "Content-Type")
                
                let imageData = obj as! Data
                var data = Data()
                data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)

                data.append("Content-Disposition: form-data; name=\"fileUpload\"; filename=\"\(boundary).jpeg\"\r\n".data(using: .utf8)!)
                data.append("Content-Type: image/jpeg\r\n\r\n".data(using: .utf8)!)
                data.append(imageData)
                
                data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)

                request.addValue(String(data.count), forHTTPHeaderField: "Content-Length")
                hashing.append("\(data.count)")
                request.httpBody = data
            }
            else {
                fatalError("Unsupported payload type for PUT \(String(describing: payload.self))")
            }
        default: break
        }
        
        hashing.append(token ?? "")
        
        let hash2 = helper_hashing.hashString(hashing, andKey: K.key2)!
        
        request.addValue(hash2, forHTTPHeaderField: "x-hash")
        
        return request
    }
    
    // MARK: - Public Methods
    
    public func get(path: String, retryOnNoConnection: Bool = true) -> Single<Data> {
        serverTask(path: path, method: .GET, payload: nil, timeout: K.maxTimeout, retryOnNoConnection: retryOnNoConnection)
    }
    
    public func post(path: String, payload: Encodable, retryOnNoConnection: Bool = true) -> Single<Data>  {
        serverTask(path: path, method: .POST, payload: payload, timeout: K.maxTimeout, retryOnNoConnection: retryOnNoConnection)
    }
    
    public func put(path: String, payload: Encodable, retryOnNoConnection: Bool = true) -> Single<Data>  {
        serverTask(path: path, method: .PUT, payload: payload, timeout: K.maxTimeout, retryOnNoConnection: retryOnNoConnection)
    }
    
    // MARK: - Private Methods
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    //MARK: - Token Related
    
    // Constants
    let kAccessTokenKey = "access_token"
    let kAccessTypeKey = "access_type"
    
    var token: String?
    var token_type: String?
    
    public func updateAccessToken(model: Response) {
        token = model.access_token
        token_type = model.access_type
        NZPreferences.instance().setValue(model.access_token, forKey: kAccessTokenKey)
        NZPreferences.instance().setValue(model.access_type, forKey: kAccessTypeKey)
    }
    
    public func revokeAccessToken() {
        token = nil
        token_type = nil
        NZPreferences.instance().deleteValue(key: kAccessTokenKey)
        NZPreferences.instance().deleteValue(key: kAccessTypeKey)
    }
    
    func reloadMemoryCache() {
        token = NZPreferences.instance().value(forKey: kAccessTokenKey) as? String
        token_type = NZPreferences.instance().value(forKey: kAccessTypeKey) as? String
    }
    
    // MARK: - Token Acquisition
    func requestForTokenIfNeeded() {
        TUtilz.sync(withLock: self) { [weak self] in
            let this = self!
            if this.tokenAQDisposable == nil {
                this.tokenAQDisposable = this.requestToken().retry(strategy: .delayed(delay: .seconds(1))).subscribe(onSuccess: {  [weak this] (authResponse) in
                    let this = this!
                    
                    this.updateAccessToken(model: authResponse)
                    this.tokenAQSubject.onNext(())
                    this.tokenAQDisposable = nil
                    
                })
            }
        }
    }
    
    func requestToken() -> Single<Response> {
        return self.__serverTask(path: "/device/auth", method: .POST, payload: helper.device.info(), timeout: K.maxTimeout).flatMap { (data) -> Single<Response> in
            do {                
                let result = try JSONDecoder().decode(Response.self, from: data)
                return Single.just(result)
            } catch {
                return Single.error(NSError.app.network.authResponseDecode())
            }
        }
    }
}
