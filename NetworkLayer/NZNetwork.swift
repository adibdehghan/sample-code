//
//  NetworkRouter.swift
//  Crowded
//
//  Created by adb on 3/29/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//


import UIKit
import RxSwift

public class NZNetwork {
    
    public static func get<T, P: RawRepresentable>(path: P, retryOnNoConnetion: Bool = true, flatMap: @escaping (Data) throws -> (Single<T>)) -> Single<T> where P.RawValue == String {
        NZNetworkHandler.instance().get(path: path.rawValue, retryOnNoConnection: retryOnNoConnetion).flatMap { (data) -> PrimitiveSequence<SingleTrait, T> in
            do {
                return try flatMap(data)
            } catch {
                return Single.error(error)
            }
        }.observeOn(MainScheduler.instance)
    }
    
    public static func post<T, P: RawRepresentable>(path: P, payload: Encodable, retryOnNoConnetion: Bool = true, flatMap: @escaping (Data) throws -> (Single<T>)) -> Single<T> where P.RawValue == String {
        NZNetworkHandler.instance().post(path: path.rawValue, payload: payload, retryOnNoConnection: retryOnNoConnetion).flatMap { (data) -> PrimitiveSequence<SingleTrait, T> in
            do {
                return try flatMap(data)
            } catch {
                return Single.error(error)
            }
        }.observeOn(MainScheduler.instance)
    }
    
    public static func put<T, P: RawRepresentable>(path: P, payload: Encodable, retryOnNoConnetion: Bool = true, flatMap: @escaping (Data) throws -> (Single<T>)) -> Single<T> where P.RawValue == String {
        NZNetworkHandler.instance().put(path: path.rawValue, payload: payload, retryOnNoConnection: retryOnNoConnetion).flatMap { (data) -> PrimitiveSequence<SingleTrait, T> in
            do {
                return try flatMap(data)
            } catch {
                return Single.error(error)
            }
        }.observeOn(MainScheduler.instance)
    }
}


