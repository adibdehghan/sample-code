//
//  NetworkRouter.swift
//  Crowded
//
//  Created by adb on 3/29/20.
//  Copyright © 2020 com.nizek. All rights reserved.
//

import UIKit

public struct NetworkRouter {
    public enum routes: RawRepresentable {
        public typealias RawValue = String
        
        case categories, hotPicks, ambassador(id: Int)
        
        public var rawValue: String {
            switch self {
            case .categories: return "category"
            case .hotPicks: return "picks"
            case .ambassador(let id): return "ambassador/\(id)"
            }
        }
        
        public init?(rawValue: Self.RawValue) {
            nil
        }
         
        enum event: RawRepresentable
             {
            typealias RawValue = String
            case getList(page: Int, lat: String?,lon: String?), get(id: String),myEvents(page: Int,previous: Bool,date: Int?) ,apply, setStatus, current, feedback, leftOption, active, pause, continueEvent, left,leftFeedback, elapsed
                 
            var rawValue: String
            {
                switch self {
                case .getList(let page,let lat,let lon):
                    if lat∂ && lon∂
                    {
                        return "event?page=\(page)&lat=\(lat!)&lon=\(lon!)"
                    }
                    else
                    {
                        return "event?page=\(page)"
                    }
                case .get(let id):
                    return "event/\(id)"
                case .myEvents(let page,let previous,let date):
                    if date∂ {
                        return "event/myEvent?page=\(page)&previous=\(previous)&date=\(date!)"
                    }
                    else {
                        return "event/myEvent?page=\(page)&previous=\(previous)"
                    }
                case .apply: return "event"
                case .active: return "event/active"
                case .pause: return "event/pause"
                case .continueEvent: return "event/continue"
                case .left: return "event/left"
                case .setStatus: return "event/status"
                case .current: return "event/current"
                case .feedback: return "event/feedback"
                case .leftOption: return "event/leftOption"
                case .leftFeedback: return "event/leftFeedback"
                case .elapsed: return "event/elapsed"
                }
            }
            init?(rawValue: Self.RawValue) {
                nil
            }
        }
     
    }
}
